//
// Created by artem on 15.10.23.
//

#include "header.h"
#include "image.h"
#include <stdio.h>
#include <stdlib.h>

#define TYPE 19778
#define  OFF_BITS 54
#define SIZE 40
#define PLANES 1
#define BIT_COUNT 24
#define RESERVED 0
#define COMPRESSION 0
#define X_PELS_PER_METER 0
#define Y_PELS_PER_METER 0
#define CLS_USED 0
#define CLR_IMPORTANT

struct bmp_header *alloc_header(void) {
    return malloc(sizeof(struct bmp_header));
}

struct bmp_header *create_header(const uint32_t width, const uint32_t height) {
    struct bmp_header *header = alloc_header();
    header->bfType = TYPE;
    header->bfReserved = RESERVED;
    header->bOffBits = OFF_BITS;
    header->biSize = SIZE;
    header->biWidth = width;
    header->biHeight = height;
    header->biPlanes = PLANES;
    header->biBitCount = BIT_COUNT;
    header->biCompression = COMPRESSION;
    header->biSizeImage = (sizeof(struct pixel) * width + get_padding(width)) * height;
    header->bfileSize = header->biSizeImage + sizeof(struct bmp_header);
    header->biXPelsPerMeter = X_PELS_PER_METER;
    header->biYPelsPerMeter = Y_PELS_PER_METER;
    header->biClrUsed = CLS_USED;
    header->biClrImportant = 0;
    return header;
}

struct image *create_image(struct bmp_header *header) {
    struct image *img = malloc(sizeof(struct image));
    img->height = header->biHeight;
    img->width = header->biWidth;
    img->data = calloc(header->biHeight, (sizeof(struct pixel) * header->biWidth + get_padding(header->biWidth)));
    return img;
}

enum read_status read_header(FILE *in, struct bmp_header *header) {
    if (!fread(header, sizeof(struct bmp_header), 1, in)) {
        return READ_INVALID_HEADER;
    }
    return READ_OK;
}

enum write_status write_header(FILE *out, struct bmp_header *header) {
    if (!fwrite(header, sizeof(struct bmp_header), 1, out)){
        return WRITE_ERROR;
    }
    return WRITE_OK;
}
