
#include "header.h"
#include "image.h"
#include "transform.h"
#include <stdio.h>
#include <stdlib.h>

#define COUNT_ARGS 3

int main(int argc, char **argv) {
    if (argc < COUNT_ARGS) {
        printf("Incorrect arguments.");

    } else {
        FILE *in = fopen(argv[1], "rb");
        FILE *out = fopen(argv[2], "wb");
        int degree = atoi(argv[3]);
        struct bmp_header *in_header = alloc_header();
        if (read_header(in, in_header)!=READ_OK){
            printf("Input file's header is not correct.");
        }
        struct image *in_img = create_image(in_header);
        if (from_bmp(in, in_img)!=READ_OK){
            printf("Input file is not correct.");
        };
        struct image *out_img = rotate_with_degree(in_img, degree);
        struct bmp_header *out_header;
        if (degree % 180 != 0) {
            out_header = create_header(in_header->biHeight, in_header->biWidth);
        } else {
            out_header = create_header(in_header->biWidth, in_header->biHeight);
        }
        if (write_header(out, out_header)!=WRITE_OK){
            printf("Output file is not correct.");
        };
        if (to_bmp(out, out_img)!=WRITE_OK){
            printf("Output file is not correct.");
        };


        free(in_header);
        free(in_img->data);
        free(in_img);
        free(out_img->data);
        free(out_img);
        free(out_header);
        fclose(in);
        fclose(out);
    }
    return 0;
}
