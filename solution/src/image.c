//
// Created by artem on 17.10.23.
//
#include "image.h"
#include <stdio.h>
#include <stdlib.h>

uint32_t get_padding(const uint32_t width) {
    return (4 - (width * sizeof(struct pixel)) % 4) % 4;
}

enum read_status from_bmp(FILE *in, struct image const *img) {
    if (in == NULL || img == NULL || img->width < 0 || img->height < 0) {
        return READ_INVALID_SIGNATURE;
    }
    for (int i = 0; i < img->height; i++) {
        if (!fread(img->data + i * img->width, sizeof (struct pixel), img->width, in)) {
            return READ_INVALID_BITS;
        }
        if (fseek(in, get_padding(img->width), SEEK_CUR)) {
            return READ_INVALID_BITS;
        }

    }
    return READ_OK;
}

enum write_status to_bmp(FILE *out, struct image const *img) {
    for (int i = 0; i < img->height; i++) {
        if (!fwrite(img->data + i * img->width, sizeof (struct pixel), img->width, out)) {
            return WRITE_ERROR;
        }
        if (!fwrite(img->data + i * img->width, get_padding(img->width), 1, out) && get_padding(img->width) != 0) {
            return WRITE_ERROR;
        }

    }

    return WRITE_OK;
}


