//
// Created by artem on 18.10.23.
//
#include "transform.h"
#include "image.h"
#include <malloc.h>


struct  image *rotate_with_degree(struct  image const *source, int degree){
    if (degree > 360){
        degree = degree % 360;
    }
    int count  = ((360-degree)%360)/90;
    if (count==0){
        struct image *img = malloc(sizeof(struct image));
        img->data = calloc(source->width*source->height,sizeof (struct pixel));
        img->height = source->height;
        img->width = source->width;
        for (uint32_t i = 0; i < source->height; i++){
            for (uint32_t j = 0; j < source -> width; j++){
                img->data[i*(source->width)+j] = source->data[i*(source->width)+j];
            }
        }
        return img;
    }
    struct image *img = rotate(source);
    for (int i = 1; i < count; i++){
        struct image *free_img = img;
        img = rotate(img);
        free(free_img->data);
        free(free_img);
    }
    return img;
}

struct image *rotate( struct image const *source ){
    struct image *img = malloc(sizeof(struct image));
    img->data = calloc(source->width*source->height,sizeof (struct pixel));
    img->height = source->width;
    img->width = source->height;
    for (uint32_t i = 0; i < source->height; i++){
        for (uint32_t j = 0; j < source -> width; j++){
            img->data[(j+1)*(source->height)-i-1] = source->data[i*(source->width)+j];
        }
    }
    return img;
}
