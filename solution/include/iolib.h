//
// Created by artem on 17.10.23.
//

#ifndef IMAGE_TRANSFORMER_IOLIB_H
#define IMAGE_TRANSFORMER_IOLIB_H

#include <bits/types/FILE.h>

/*  deserializer   */
enum read_status  {
    READ_OK = 0,
    READ_INVALID_SIGNATURE,
    READ_INVALID_BITS,
    READ_INVALID_HEADER
};

/*  serializer   */
enum  write_status  {
    WRITE_OK = 0,
    WRITE_ERROR
};



#endif //IMAGE_TRANSFORMER_IOLIB_H
