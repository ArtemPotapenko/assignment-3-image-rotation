//
// Created by artem on 15.10.23.
//

#include "iolib.h"
#include <bits/types/FILE.h>
#include <stdint.h>

#ifndef ASSIGNMENT_3_IMAGE_ROTATION_HEADER_H
#define ASSIGNMENT_3_IMAGE_ROTATION_HEADER_H
#pragma pack(push, 1)
struct bmp_header
{
    uint16_t bfType;
    uint32_t  bfileSize;
    uint32_t bfReserved;
    uint32_t bOffBits;
    uint32_t biSize;
    uint32_t biWidth;
    uint32_t  biHeight;
    uint16_t  biPlanes;
    uint16_t biBitCount;
    uint32_t biCompression;
    uint32_t biSizeImage;
    uint32_t biXPelsPerMeter;
    uint32_t biYPelsPerMeter;
    uint32_t biClrUsed;
    uint32_t  biClrImportant;
};
#pragma pack(pop)


enum read_status read_header(FILE *in, struct bmp_header *header);
enum write_status write_header(FILE *out,struct bmp_header *header);
struct bmp_header *alloc_header(void );
struct bmp_header *create_header(uint32_t width,uint32_t height);
struct image *create_image(struct bmp_header *header);
#endif //ASSIGNMENT_3_IMAGE_ROTATION_HEADER_H
