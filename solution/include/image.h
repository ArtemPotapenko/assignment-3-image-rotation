//
// Created by artem on 17.10.23.
//
#include "iolib.h"
#include <bits/types/FILE.h>
#include <stdint.h>

#ifndef IMAGE_TRANSFORMER_IMAGE_H
#define IMAGE_TRANSFORMER_IMAGE_H

#pragma pack(push, 1)
struct image {
    uint64_t width, height;
    struct pixel* data;
};
#pragma pack(pop)

 struct pixel{
     uint8_t r,g,b;
 };



enum read_status from_bmp( FILE* in, struct image const* img );

enum write_status to_bmp( FILE* out, struct image const* img );

uint32_t get_padding(uint32_t width);
#endif //IMAGE_TRANSFORMER_IMAGE_H
