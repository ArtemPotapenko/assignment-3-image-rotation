//
// Created by artem on 17.10.23.
//

#ifndef IMAGE_TRANSFORMER_TRANSFORM_H
#define IMAGE_TRANSFORMER_TRANSFORM_H
    struct image *rotate_with_degree(struct  image const *source, int degree);
    struct image *rotate( struct image const *source );
#endif //IMAGE_TRANSFORMER_TRANSFORM_H
